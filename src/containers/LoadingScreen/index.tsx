import React from 'react';
import { LoadingScreenContainer } from './style';

export const LoadingScreen = () => {
  return (
    <LoadingScreenContainer>
      <div className="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </LoadingScreenContainer>
  );
};
