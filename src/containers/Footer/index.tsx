import React from 'react';
import { FooterContainer } from './style';
import { FooterComp } from '../../component/FooterComponent';

export const Footer = () => {
  return (
    <FooterContainer>
      <FooterComp></FooterComp>
    </FooterContainer>
  );
};
