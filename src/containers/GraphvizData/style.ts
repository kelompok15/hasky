import styled from 'styled-components';

export const GraphvizDataContainer = styled.div`
margin: 5rem;

  h1 {
    color: white;
  }

  .content {
    display: flex;
    justify-content: center;
  }
`;
