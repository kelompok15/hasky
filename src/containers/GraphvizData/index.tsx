import React from 'react';
import { GraphvizDataContainer } from './style';
import { Graphviz } from 'graphviz-react';
import { generateGraphForm } from '../../utils/generateGraphForm';

type GraphvizDataProps = {
  name: string,
  data: string[]
}

export const GraphvizData = ({ name, data } : GraphvizDataProps) => {
  return (
    <GraphvizDataContainer >
      <h1>{name}</h1>
      <div className="content">
        <Graphviz dot={generateGraphForm(data)} options={{
          width: 500,
          zoom: false,
          fade: true
        }}/>
      </div>
    </GraphvizDataContainer>
  );
};
