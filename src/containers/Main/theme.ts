import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  borderRadius: '5px',

  fontSize: {
    h1: '24px',
    h2: '18px',
    p: '14px',
    a: '14px',
  },

  colors: {
    main: 'cyan',
    secondary: 'magenta',
  },
};

export { theme };
