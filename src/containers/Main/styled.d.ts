// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    borderRadius: string;

    fontSize: {
      h1: string;
      h2: string;
      p: string;
      a: string;
    };

    colors: {
      main: string;
      secondary: string;
    };
  }
}
