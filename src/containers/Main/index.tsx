import React from 'react';
// import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { HomePage } from '../HomePage';
import { ResultPage } from '../ResultPage';
import { NotFoundPage } from '../NotFoundPage';
// import { MainPage } from './styled';
import { Navbar } from '../Navbar';
import { Footer } from '../Footer';
// import { routes } from './routes';
import { useRoutes } from 'hookrouter';

// const mainRoutes = routes.map((route, idx) => <Route key={`${idx}${route.path}`} {...route} />);

interface IdataProps {
  data: string;
  requestedData: string;
}

const routes = {
  '/': () => <HomePage />,
  '/result/:resultData/:requestedData': ({ resultData, requestedData }: { [key: string]: any }) => <ResultPage data={resultData} requestedData={requestedData} />,
};

const Main: React.FC = () => {
  const route = useRoutes(routes);
  // return (
  //   <Router>
  //     <Switch>{mainRoutes}</Switch>
  //   </Router>
  // );
  return (
    <div>
      <Navbar />
      {route || <NotFoundPage />}
      <Footer />
    </div>
  );
};

export default Main;
