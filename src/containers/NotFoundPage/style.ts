import styled from 'styled-components';

export const NotFoundPageContainer = styled.div`
@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");
height: 90vh;
display: flex;
justify-content: center;
align-items: center;
text-align: center;
flex-direction: column;

  h1 {
    margin: 0;
  }

`;
