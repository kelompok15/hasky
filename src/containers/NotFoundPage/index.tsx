import React from 'react';
import { NotFoundPageContainer } from './style'

export const NotFoundPage: React.FC = () => {
    return (
        <NotFoundPageContainer>
            <h2>Are you Lost?</h2>
            <h1>404 Page Not Found</h1>
        </NotFoundPageContainer>
    );
};
