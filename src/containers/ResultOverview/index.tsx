import React from 'react';
import { TableStyle } from './style';

type ResultCompProps = {
    financialModel: string,
    pricing: {
        currentPrice: string,
        strikePrice: string,
    }
    rates: {
        maxIR: string,
        maxDR: string,
        interestRate: string,
    }
    horizon: string
}


export const ResultOComp = ({ financialModel, pricing, rates, horizon }: ResultCompProps) => {
    return (
        <TableStyle>
            <table cellSpacing="0">
                <tr>
                    <th>Financial Model</th>
                    <th>Pricing</th>
                    <th>Rates</th>
                    <th>Horizon</th>
                </tr>
                <tr>
                    <td rowSpan={2}>{dictFinancialModel[financialModel]}</td>
                    <td>Current Price: {pricing.currentPrice}</td>
                    <td>Max Increasing Rate: {rates.maxIR}</td>
                    <td rowSpan={2}>{horizon}</td>
                </tr>
                <tr>
                    <td>Strike Price: {pricing.strikePrice}</td>
                    <td>Max Decreasing Rate: {rates.maxDR}</td>
                </tr>
            </table>
        </TableStyle>
    )
}
export const dictFinancialModel: { [id: string]: any } = {
    'zcb': 'Zero Coupon Bond',
    'european': 'European',
    'american': 'American',
}