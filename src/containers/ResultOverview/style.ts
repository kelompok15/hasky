import styled from 'styled-components';

export const TableStyle = styled.div`
  color: black;
  margin-top: 100px;
  text-align: center;

  table {
    margin:auto;
    background-color: white;
    width: 90%;
    height: 180px;
    border-radius:10px;
  }

  th {
    width: 150px;
    border-bottom: 1px solid #95969B;
    margin: 0px;
    height:50px;
    color: #6247AA;
  }

  td {
    text-align: center;
    font-size: 13px;
  }
`;