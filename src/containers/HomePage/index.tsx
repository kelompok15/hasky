import React from 'react';
import { LandingPage, StepPage } from '../App/Containers'

export const HomePage: React.FC = () => {
    return (
        <div>
            <LandingPage />
            <StepPage />
        </div>
    );
};
