import React from 'react';
import { BackButtonComp } from '../../component/BackButton';
import { ResultPage as ResPage } from './style';
import { ResultOComp } from '../ResultOverview';
import { TreeData } from '../TreeData';
import { GraphvizData } from '../GraphvizData';

type ResultPageProps = {
  data: string;
  requestedData: string;
};

export const ResultPage = (data: ResultPageProps) => {
  console.log(data.data.split(","));
  const cleanDataArray = data.data.split(",").map(e => String(Number(e).toFixed(3)));
  console.log(cleanDataArray);
  const cleanReqData = JSON.parse(decodeURI(data.requestedData));

  const financialModelValue = cleanReqData['financialModel'];
  const pricingValue = cleanReqData['pricing'];
  const ratesValue = cleanReqData['rates'];
  const horizonValue = cleanReqData['horizon'];

  console.log(cleanReqData);

  return (
    <ResPage>
      <BackButtonComp title='< Back'></BackButtonComp>
      <ResultOComp financialModel={financialModelValue} pricing={pricingValue} rates={ratesValue} horizon={horizonValue} ></ResultOComp>
      <GraphvizData name={"Option Price Today"} data={cleanDataArray}/>
    </ResPage>
  );
};

// <div className="resultData">
//   {Object.keys(cleanData).map(data => <TreeData name={data} data={cleanData[String(data)]} />)}
// </div>
