import styled from 'styled-components';

export const ResultPage = styled.div`
  background-color: #30333D;
  padding: 20px 20px;
  font-family: "Poppins";
  @import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");
  height: 100rem;

  a {
    color:white;
    font-size: 25px;
    text-decoration:none;
  }

  .resultData {
    width: 100%;
    display: flex;
    flex-direction: column;
    margin: 5rem;
  }
`;
