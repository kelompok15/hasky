import React from 'react';
import Tree from 'react-tree-graph';
import { TreeDataContainer } from './style';
import 'react-tree-graph/dist/style.css';

type TreeDataProps = {
  name: string,
  data: object
}

export const TreeData = ({ name, data } : TreeDataProps) => {
  return (
    <TreeDataContainer >
      <h1>{name.split("_").map(text => text.charAt(0).toUpperCase() + text.slice(1)).join(" ")}</h1>
      <Tree
        data={data}
        nodeRadius={15}
        margins={{ top: 20, bottom: 10, left: 20, right: 200 }}
        height={500}
        width={800}
        animated
      />
    </TreeDataContainer>
  );
};
