import styled from 'styled-components';

export const TreeDataContainer = styled.div`
margin: 2rem;

  h1 {
    color: white;
  }

  .node circle {
    fill: #8953e2;
    stroke: white;
    stroke-width: 1.5px;
  }

  .node text {
    font-size: 14px;
    fill: white;
  }

  .node {
    cursor: pointer;
  }

  path.link {
    fill: none;
    stroke: rgb(157, 221, 203);
    stroke-width: 1.5px;
  }

  body {
    background-color: white;
    overflow: hidden;
  }

  span {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    color: #f4f4f4;
    text-shadow: 0 1px 4px black;
    float: right;
  }
`;
