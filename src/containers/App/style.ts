import styled from 'styled-components';

export const AppContainer = styled.div`
  .App {
    text-align: center;
  }

  .App-logo {
    height: 40vmin;
  }

  .App-header {
    background-color: #282c34;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: calc(10px + 2vmin);
    color: white;
  }

  .App-link {
    color: #09d3ac;
  }
`;

export const LandingPageContainer = styled.div`
  .App {
    text-align: center;
    background-color: #30333d;
    color: white;
    height: 150px;
    font-family: 'Poppins', sans-serif;
    padding: 190px;
  }

  .Title {
    margin: 0 0 40px 0;
  }

  .TitleH2 {
    margin-top: 40px;
    width: 485px;
    margin: auto;
    margin-bottom: 20px;
  }

  @fontface {
    font-family: 'Poppins', sans-serif;
    src: url('https://fonts.googleapis.com/css?family=Poppins&display=swap');
  }
`;

export const StepPageContainer = styled.div`
display: flex;
flex-direction: column;
align-items: center;
margin: 2rem 0 10rem 0;

  .Flex {
    display: flex;
    padding: 150px 45px;
  }

  .InputGroup {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    padding: 0 2rem;
  }

  .InputGroup > div {
    flex: 0 0 50%;
  }

  .SliderWrapper {
    width: 100%;
  }

  .RightGroup {
    display: flex;
    justify-content: space-between;
    text-align: center;
    width: 100%;
  }

  .ticket {
    fill: blue;
  }

  .LeftGroup {
    width: 45%;
    height: 100px;
  }
`;

export const MainPage = styled.div`
  h1 {
  }
`;
