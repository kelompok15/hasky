import React from 'react';
import logo from 'assets/logo.svg';
import { AppContainer } from './style';
import { HaskyButton } from '../../component/HaskyButton';
import { InputStep } from '../InputStep';
import { Navbar } from '../Navbar';
import { Footer } from '../Footer';

const App: React.FC = () => {
  return (
    <AppContainer>
      <Navbar />
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </a>
          <HaskyButton name="Calculate" width="15rem" height="4rem" font_size="1.5rem"/>
        </header>
        <InputStep step={1} maxStep={4} name="Financial Model" >
          <p>Hehehhe</p>
        </InputStep>
        <InputStep step={2} maxStep={4} name="Pricing" >
          <p>Hehehhe</p>
        </InputStep>
        <InputStep step={3} maxStep={4} name="Rates" >
          <p>Hehehhe</p>
        </InputStep>
        <InputStep step={4} maxStep={4} name="Horizon" >
          <p>Hehehhe</p>
        </InputStep>
      </div>
      <Footer />
    </AppContainer>
  );
};

export default App;
