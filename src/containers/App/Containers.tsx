import React, { useState } from 'react';
import axios from 'axios';
import { navigate } from 'hookrouter';
import { LandingPageContainer, StepPageContainer } from './style';
import { HaskyButton } from '../../component/HaskyButton';
import { InputStep } from '../InputStep';
import { LoadingScreen } from '../LoadingScreen';
import { BoxComp } from '../../component/Box';
import ticket from '../../assets/logo/ticket-grey.svg';
import european from '../../assets/logo/european-grey.svg';
import american from '../../assets/logo/american-grey.svg';
import InputField from '../../component/InputField';
import InputDragBar from '../../component/InputDragBar';
import { AMERICAN_COUPON_BOND_API } from '../../api';

export const LandingPage = () => {
  return (
    <LandingPageContainer>
      <div className="App">
        <h1 className="Title">Calculate my Contract</h1>
        <p className="TitleH2">
          Deciding to buy a contract or not is very tricky. Miscalculating can cause you a lot of money. Tell us the
          variables and we will calculate it for you
        </p>
        <HaskyButton name="Calculate" width="200px" height="50px" font_size="16px" />
      </div>
    </LandingPageContainer>
  );
};

export const StepPage = () => {
  const FINANCIAL_MODEL = 'financialModel';
  const CURRENT_PRICE = 'currentPrice';
  const STRIKE_PRICE = 'strikePrice';
  const MAX_INCREASING_RATE = 'maxIncreasingRate';
  const MAX_DECREASING_RATE = 'maxDecreasingRate';
  const INTEREST_RATE = 'interestRate';
  const HORIZON = 'horizon';

  const ZERO_COUPON_ID = 0;
  const EUROPEAN_COUPON_ID = 1;
  const AMERICAN_COUPON_ID = 2;

  const [isLoading, setIsLoading] = useState(false);

  const initialValuesObject: {
    [key: string]: string | number;
  } = {
    [FINANCIAL_MODEL]: 0,
    [CURRENT_PRICE]: '',
    [STRIKE_PRICE]: '',
    [MAX_INCREASING_RATE]: '',
    [MAX_DECREASING_RATE]: '',
    [INTEREST_RATE]: '',
    [HORIZON]: 4,
  };

  const initialFormValidationObject: {
    [key: string]: boolean;
  } = {
    [FINANCIAL_MODEL]: false,
    [CURRENT_PRICE]: false,
    [STRIKE_PRICE]: false,
    [MAX_INCREASING_RATE]: false,
    [MAX_DECREASING_RATE]: false,
    [INTEREST_RATE]: false,
  };

  const [formValues, setFormValues] = useState(initialValuesObject);

  const [formValidationWarning, setFormValidationWarning] = useState(initialFormValidationObject);

  const financialModelURLLookUp: { [id: number]: string } = {
    0: 'https://d55ykgd34g.execute-api.us-east-1.amazonaws.com/default/zcbFunction', // ZCB
    1: 'https://gtdkrriri0.execute-api.us-east-1.amazonaws.com/default/EuropeanNonBarrierPutFunction', // EUROPEAN NON BARRIER
    2: 'https://ihb6rjlkzi.execute-api.us-east-1.amazonaws.com/default/americanFunction', // AMERICAN
  };

  const financialModelLookUp: { [id: number]: string } = {
    0: 'zcb', // ZCB
    1: 'european', // EUROPEAN NON BARRIER
    2: 'american', // AMERIAN
  };

  const setState = (field: string) => (value: string | number): void =>
    setFormValues(prevState => ({ ...prevState, [field]: value }));

  const checkFieldValueValidity = (field: string, value: string | number): boolean => {
    if (typeof value === 'string' && value.length === 0) {
      return false;
    } else {
      if (value < 0) {
        return false;
      }
    }

    if (field === FINANCIAL_MODEL) {
      if (value !== 0 && value !== 1 && value !== 2) {
        return false;
      }
    }

    return true;
  };

  const validateForm = (): boolean => {
    const newFormValidation = { ...formValidationWarning };

    Object.keys(formValues).map(field => {
      if (!checkFieldValueValidity(field, formValues[field])) {
        newFormValidation[field] = true;
      } else {
        newFormValidation[field] = false;
      }
    });

    setFormValidationWarning(newFormValidation);

    let isValid = true;

    Object.keys(newFormValidation).map(field => {
      if (newFormValidation[field]) {
        isValid = false;
      }
    });

    return isValid;
  };

  const getToSendObject = (): object => {
    return formValues[FINANCIAL_MODEL] === 0
      ? {
          faceValue: Number(formValues[CURRENT_PRICE]),
          maxRate: Number(formValues[MAX_INCREASING_RATE]) / 100,
          minRate: Number(formValues[MAX_DECREASING_RATE]) / 100,
          interestRate: Number(formValues[INTEREST_RATE]) / 100,
          maturityDate: Number(formValues[HORIZON]),
        }
      : {
          stockPrice: Number(formValues[CURRENT_PRICE]),
          strikePrice: Number(formValues[STRIKE_PRICE]),
          volatility: Number(formValues[INTEREST_RATE]) / 100,
          maxRate: Number(formValues[MAX_INCREASING_RATE]) / 100,
          minRate: Number(formValues[MAX_DECREASING_RATE]) / 100,
          yearsToMaturity: Number(formValues[HORIZON]),
        };
  };

  const requestApi = (apiUrl: string, formData: object, formResult: object): void => {
    setIsLoading(true);
    axios
      .post(apiUrl, formData)
      .then(res => {
        setIsLoading(false);
        console.log('Success');
        console.log(res.data);
        navigate(`/result/${res.data}/${encodeURI(JSON.stringify(formResult))}`);
      })
      .catch(err => {
        setIsLoading(false);
        // swall(error);
        console.log('Error in axios post');
      });
  };

  const calculate = (): void => {
    const {
      financialModel,
      currentPrice,
      strikePrice,
      maxIncreasingRate,
      maxDecreasingRate,
      interestRate,
    } = formValues;
    const financialModelURL = financialModelURLLookUp[Number(financialModel)];
    const financialModelValue = financialModelLookUp[Number(financialModel)];
    const currentPriceValue = Number(currentPrice);
    const strikePriceValue = Number(strikePrice);
    const maxIncreasingRateValue = Number(maxIncreasingRate);
    const maxDecreasingRateValue = Number(maxDecreasingRate);
    const interestRateValue = Number(interestRate);

    const requestedData = {
      financialModel: financialModelValue,
      pricing: {
        currentPrice: currentPriceValue,
        strikePrice: strikePriceValue,
      },
      rates: {
        maxIR: maxIncreasingRateValue,
        maxDR: maxDecreasingRateValue,
        interestRate: interestRateValue,
      },
      horizon: formValues[HORIZON],
    };

    const isValid = validateForm();

    if (isValid) {
      requestApi(financialModelURL, getToSendObject(), requestedData);
    }
  };

  return (
    <StepPageContainer>
      {isLoading && <LoadingScreen />}
      <InputStep step={1} maxStep={4} name="Financial Model">
        <div className="RightGroup">
          <BoxComp
            idSrc={ZERO_COUPON_ID}
            title="Zero Coupon Bond"
            imgSrc={ticket}
            selected={Number(formValues[FINANCIAL_MODEL])}
            onSelect={setState(FINANCIAL_MODEL)}
          ></BoxComp>
          <BoxComp
            idSrc={EUROPEAN_COUPON_ID}
            title="European"
            imgSrc={european}
            selected={Number(formValues[FINANCIAL_MODEL])}
            onSelect={setState(FINANCIAL_MODEL)}
          ></BoxComp>
          <BoxComp
            idSrc={AMERICAN_COUPON_ID}
            title="American"
            imgSrc={american}
            selected={Number(formValues[FINANCIAL_MODEL])}
            onSelect={setState(FINANCIAL_MODEL)}
          ></BoxComp>
        </div>
      </InputStep>
      <InputStep step={2} maxStep={4} name="Pricing">
        <div className="InputGroup">
          <InputField
            name="Current price:"
            value={String(formValues[CURRENT_PRICE])}
            validation={formValidationWarning[CURRENT_PRICE]}
            onChange={setState(CURRENT_PRICE)}
          />
          <InputField
            name="Strike price:"
            value={String(formValues[STRIKE_PRICE])}
            validation={formValidationWarning[STRIKE_PRICE]}
            onChange={setState(STRIKE_PRICE)}
          />
        </div>
      </InputStep>
      <InputStep step={3} maxStep={4} name="Rates">
        <div className="InputGroup">
          <InputField
            name="Max Increasing Rate:"
            value={String(formValues[MAX_INCREASING_RATE])}
            validation={formValidationWarning[MAX_INCREASING_RATE]}
            onChange={setState(MAX_INCREASING_RATE)}
          />
          <InputField
            name="Max Decreasing Rate:"
            value={String(formValues[MAX_DECREASING_RATE])}
            validation={formValidationWarning[MAX_DECREASING_RATE]}
            onChange={setState(MAX_DECREASING_RATE)}
          />
          <InputField
            name="Interest Rate:"
            value={String(formValues[INTEREST_RATE])}
            validation={formValidationWarning[INTEREST_RATE]}
            onChange={setState(INTEREST_RATE)}
          />
        </div>
      </InputStep>
      <InputStep step={4} maxStep={4} name="Horizon">
        <div className="SliderWrapper">
          <InputDragBar min={1} max={10} value={Number(formValues[HORIZON])} onChange={setState(HORIZON)} />
        </div>
      </InputStep>
      <HaskyButton name="Calculate" width="15rem" height="4rem" font_size="1.5rem" onClick={calculate} />
    </StepPageContainer>
  );
};
