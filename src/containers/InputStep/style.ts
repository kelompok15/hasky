import styled from 'styled-components';

export const InputStepContainer = styled.div`
@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");
display: flex;
flex-direction: row;
width: 100%;
justify-content: center;
align-items: center;
margin: 5rem;
font-family: "Poppins";

  .title, .inputSection {
    margin: 2rem;
  }

  .title {
    width: 20%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-end;
  }

  .progress {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    text-align: end;
    color: #BDEDE0;
  }

  h1 {
    margin: 0;
  }

  .name {
    width: 4rem;
    color: #6247AA;
    display: flex;
    justify-content: flex-end;
    text-align: end;
  }

  .inputSection {
    width: 80%;
    height: 100%;
    display: flex;
    padding: 2rem;
  }
`;
