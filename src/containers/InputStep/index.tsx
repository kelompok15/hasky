import React from 'react';
import { InputStepContainer } from './style';
import { TitleComp } from '../../component/Title';

type InputStepProps = {
  step: number,
  maxStep: number,
  name: string,
  children: JSX.Element
}

export const InputStep = ({ step, maxStep, name, children }: InputStepProps) => {
  return (
    <InputStepContainer >
      <div className="title">
        <TitleComp step={step} maxStep={maxStep} name={name}></TitleComp>
      </div>
      <div className="inputSection">
        {children}
      </div>
    </InputStepContainer>
  );
};
