import styled from 'styled-components';

export const NavbarContainer = styled.div`
@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");
background: #30333D;
display: flex;
flex-direction: row;
justify-content: space-between;
position: sticky;
top: 0;
z-index: 100;
border-bottom: 0.25px solid rgba(250, 250, 250, 0.75);

  .logoSection {
    width: 5rem;
    padding: 0.75rem 1rem;
  }

  .logoSection img {
    width: 2rem;
  }

  .optionsSection {
    font-family: "Poppins";
    height: auto;
    display: flex;
    align-items: center;
    border-bottom: 2px solid white;
  }

  .optionsSection h4 {
    color: white;
    font-size: 0.75rem;
    font-weight: 600;
  }

  .contactSection {
    width: 5rem;
    padding: 1rem;
  }

`;
