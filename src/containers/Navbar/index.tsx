import React from 'react';
import { NavbarContainer } from './style';
import HaskyLogo from '../../assets/hasky-logo.png';
import { HaskyButton } from '../../component/HaskyButton';
import { LogoComp, OptionsSectionComp, ContactSectionComp } from '../../component/NavbarComponent';

export const Navbar = () => {
  return (
    <NavbarContainer>
      <LogoComp></LogoComp>
      <OptionsSectionComp></OptionsSectionComp>
      <ContactSectionComp></ContactSectionComp>
    </NavbarContainer>
  );
};
