import styled from 'styled-components';

export const InputFieldContainer = styled.div`
  .inputText {
    margin-left: 0.5rem;
    color: #3d3d3d;
  }

  .inputStyle {
    width: 18rem;
    height: 3rem;
    padding: 1rem;
    border-radius: 10px;
    border: none;
    font-size: 2rem;
    box-shadow: 0px 4px 50px rgba(0, 0, 0, 0.05);
  }

  input: focus {
    outline: none;
    box-shadow: 0px 20px 50px #bdede0;
  }
`;
