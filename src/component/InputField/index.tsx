import React from 'react';
import { InputFieldContainer } from './style';
import ValidationMessage from '../ValidationMessage';

const InputField: React.FunctionComponent<InputFieldProps> = ({ name, value, onChange, validation }) => (
  <InputFieldContainer>
    <h2 className="inputText">{name}</h2>
    <input className="inputStyle" type="number" value={value} onChange={e => onChange(e.target.value)} />
    <ValidationMessage isShowed={validation} text={'Please input value >= 0'} />
  </InputFieldContainer>
);

type InputFieldProps = {
  name: string;
  value: string;
  onChange: Function;
  validation: boolean;
};

export default InputField;
