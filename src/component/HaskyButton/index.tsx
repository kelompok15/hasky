import React from 'react';
import { HaskyButtonContainer } from './style';

type HaskyButtonProps = {
  name: string,
  width?: string,
  height?: string,
  font_size?: string,
  onClick?: () => void
}

export const HaskyButton = ({ name, width, height, font_size, onClick }: HaskyButtonProps) => {
  return (
    <HaskyButtonContainer width={width} height={height} font_size={font_size}>
      <button onClick={onClick}>
        {name}
      </button>
    </HaskyButtonContainer>
  );
};
