import styled from 'styled-components';

type HaskyButtonContainerProps = {
  width?: string,
  height?: string,
  font_size?: string
}

export const HaskyButtonContainer = styled.div<HaskyButtonContainerProps>`
@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");

  button {
    width: ${props => props.width ? props.width : "10rem"};
    height: ${props => props.height ? props.height : "3rem"};
    padding: 0.5rem;
    text-align: center;
    transition: 0.5s;
    background-size: 200% auto;
    color: white;
    border-radius: 0.5rem;
    font-family: "Poppins";
    font-size: ${props => props.font_size ? props.font_size : "1rem"};
    font-weight: 500;
    background-image: linear-gradient(to right, #C0B2D8 0%, #8953E2 51%, #C0B2D8 100%);
    border: 0;
  }

  button:hover {
    background-position: right center; /* change the direction of the change here */
    cursor:pointer;
  }

  button:focus {
    outline: 0;
  }
`;
