import { BBComponent } from "./style"
import * as React from 'react';

export type BackButtonProps = {
    title: string,
}

export const BackButtonComp = ({ title }: BackButtonProps) => {
    return (
        <BBComponent>
            <a href='/'>{title}</a>
        </BBComponent>
    )
}