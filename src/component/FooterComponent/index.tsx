import React from 'react';
import { FooterComponent } from './style';

export const FooterComp = () => {
  return (
    <FooterComponent>
      <p>© Kelompok 15 Functional Programming 2019 </p>
    </FooterComponent>
  );
};
