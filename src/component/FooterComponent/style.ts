import styled from 'styled-components';

export const FooterComponent = styled.div`
  @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap');
  background: #30333d;
  height: 2rem;
  padding: 0.5rem;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;

  p {
    font-family: 'Poppins';
    font-size: 0.75rem;
    color: white;
    margin: 0;
    text-align: center;
  }
`;
