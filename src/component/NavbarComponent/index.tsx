import React from 'react';
import { NavbarComponent } from './style';
import HaskyLogo from '../../assets/hasky-logo.png';
import { HaskyButton } from '../../component/HaskyButton';


export const LogoComp = () => {
    return (
        <NavbarComponent>
            <div className="logoSection">
                <img src={HaskyLogo} />
            </div>
        </NavbarComponent>
    );
};

export const OptionsSectionComp = () => {
    return (
        <NavbarComponent>
            <div className="optionsSection">
                <h4>CALCULATE CONTRACT</h4>
            </div>
        </NavbarComponent>
    );
};


export const ContactSectionComp = () => {
    return (
        <NavbarComponent>
            <div className="contactSection">
                <HaskyButton name="Contact" width="5rem" height="2rem" font_size="0.75rem" />
            </div>
        </NavbarComponent>
    );
};