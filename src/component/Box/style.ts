import styled from 'styled-components';
import ticket from '../../assets/logo/ticket.svg'
import european from '../../assets/logo/european.svg'
import american from '../../assets/logo/american.svg'

type BoxComponentProps = {
  imgSrc?: any,
  selected: boolean,
}

export const BoxComponent = styled.div<BoxComponentProps>`
@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap");
margin: 2rem;

  .Box{
    width: 150px;
    height: 150px;

    background: #FFFFFF;
    box-shadow: 0px 4px 50px rgba(0, 0, 0, 0.05);
    border-radius: 20px;
    position:relative;

    margin: 0px 10px 0px 10px;
    justify-content: space-evenly;

    padding:50px;
  }

  .Box svg {
    fill: ${props => props.selected ? '#6247AA' : "#E4E4E4"}
  }

  .Box:hover {
    cursor:pointer;
    box-shadow: 0px 20px 50px #BDEDE0;

    svg {
      fill: #6247AA;
    }

  }


  #ticket:hover{
    img {
        content:url(${ticket});
      };
  }

  #european:hover {
    img {
        content:url(${european});
      };
  }

  #american:hover {
      img {
          content:url(${american});
      };
  }

  .Title {
    position: absolute;
    bottom: 0;
    right: 0;
    width:100%;
    text-align:center;
    margin-bottom:40px;
    color: #6247AA;
    font-weight:700;
  }
`;
