import React, { useState } from 'react';
import { InputDragBarContainer } from './style';
import Slider from '@material-ui/core/Slider';
import { withStyles } from '@material-ui/styles';

type InputDragBarProps = {
  min: number;
  max: number;
  value: number;
  onChange: Function;
};

const InputDragBar: React.FC<InputDragBarProps> = ({ min, max, value, onChange }) => {
  // const [value, setValue] = useState<number>(4);
  const customShadow = '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

  const showValue = (): string => `${value} years ahead`;

  const CustomSlider = withStyles({
    root: {
      color: '#8953E2',
      height: 8,
    },
    thumb: {
      height: 28,
      width: 28,
      backgroundColor: '#fff',
      boxShadow: customShadow,
      marginTop: -14,
      marginLeft: -14,
      '&:focus,&:hover,&$active': {
        boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
        // Reset on touch devices, it doesn't add specificity
        '@media (hover: none)': {
          boxShadow: customShadow,
        },
      },
    },
    active: {},
    valueLabel: {
      left: 'calc(-50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
      background: 'linear-gradient(90deg, rgba(121,9,104,1) 0%, rgba(98,9,121,1) 44%, rgba(0,221,255,1) 100%)',
    },
  })(Slider);

  return (
    <InputDragBarContainer>
      <div className="sliderContainer">
        <CustomSlider
          value={value}
          onChange={(_, value): void => {
            onChange(value as number);
          }}
          min={min}
          max={max}
        />
        <h2 className="inputText">{showValue()}</h2>
      </div>
    </InputDragBarContainer>
  );
};

export default InputDragBar;
