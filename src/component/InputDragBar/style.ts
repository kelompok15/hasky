import styled from 'styled-components';

export const InputDragBarContainer = styled.div`
  display: flex;
  width: 100%;
}

.sliderContainer {
  width: 100%;
  padding: 1rem;
}

.inputText {
  color: #694DB7;
}
`;
