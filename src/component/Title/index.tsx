import * as React from 'react';
import { TitleComponent } from './style';

type InputStepProps = {
    step: number,
    maxStep: number,
    name: string,
}

export const TitleComp = ({ step, maxStep, name }: InputStepProps) => {
    return (
        <TitleComponent>
            <div className="progress">
                <h1>{step}</h1>
                <h1>/</h1>
                <h1>{maxStep}</h1>
            </div>
            <div className="name">
                <h1>{name}</h1>
            </div>
        </TitleComponent>
    )
}