import React from 'react';
import { ValidationMessageContainer } from './style';

const ValidationMessage: React.FunctionComponent<ValidationMessageProps> = ({ isShowed, text }) => (
  <ValidationMessageContainer style={!isShowed ? { visibility: 'hidden' } : {}}>
    <p className="warningMessage">{text}</p>
  </ValidationMessageContainer>
);

type ValidationMessageProps = {
  isShowed: boolean;
  text: string;
};

export default ValidationMessage;
