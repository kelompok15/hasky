import styled from 'styled-components';

export const ValidationMessageContainer = styled.div`
  width: 100%;

  .warningMessage {
    color: red;
  }
`;
