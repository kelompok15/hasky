/* eslint-disable no-constant-condition */
const getLevel = (length: number): number => {
  let count = 1;

  while (length - count !== 0) {
    length -= count;
    count++;
  }

  return count - 1;
};

export const generateGraphForm = (input: string[]): string => {
  let currentKey = 0;
  let level = 1;

  const map: { [key: string]: number } = {};
  const labels: { [key: string]: Array<boolean> } = {};
  let res = '';

  while (1) {
    for (let i = 0; i < level; i++) {
      const nextPointer = currentKey + level;
      const currentVal = input[currentKey];
      const nextVal1 = input[nextPointer];
      const nextVal2 = input[nextPointer + 1];

      if (!map[currentVal]) {
        map[currentVal] = 1;
        if (!labels[currentVal]) labels[currentVal] = new Array(input.length);
      } else {
        map[currentVal]++;
      }

      if (!labels[currentVal][map[currentVal]]) {
        res += `${currentVal}${map[currentVal]} [label="${currentVal}"];\n`;
        labels[currentVal][map[currentVal]] = true;
      }

      const key = `${currentVal}${map[currentVal]}`;

      let count1 = 0;

      if (!map[nextVal1]) {
        count1 = 1;
      } else {
        count1 = map[nextVal1] + 1;
      }

      if (!labels[nextVal1]) labels[nextVal1] = new Array(input.length);

      if (!labels[nextVal1][count1]) {
        res += `${nextVal1}${count1} [label="${nextVal1}"];\n`;
        labels[nextVal1][count1] = true;
      }

      let isDuplicate = input[nextPointer] === input[nextPointer + 1];

      const value1 = `${input[nextPointer]}${count1}`;

      let count2 = 0;

      if (!map[nextVal2]) {
        count2 = 1;
      } else {
        count2 = map[nextVal2] + 1;
      }

      if (isDuplicate) {
        count2++;
        isDuplicate = false;
      }

      if (!labels[nextVal2]) labels[nextVal2] = new Array(input.length);

      if (!labels[nextVal2][count2]) {
        res += `${nextVal2}${count2} [label="${nextVal2}"];\n`;
        labels[nextVal2][count2] = true;
      }

      const value2 = `${input[nextPointer + 1]}${count2}`;

      const ans = `${key} -- { ${value1} ${value2} } [color=white];\n`;
      res += ans;

      currentKey++;
    }

    level++;

    if (level > getLevel(input.length)) {
      break;
    }
  }

  return 'graph {\nrankdir=LR;\nbgcolor="transparent";\nnode [style="rounded,filled",penwidth=0, shape=box, fontname="Poppins", color=white, fontcolor=white, fillcolor="#C0B2D8;0,8:#8953E2"]' + res + '}';
};
