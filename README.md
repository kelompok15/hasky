This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Team Members (Class B)

1. 1606875825 - Michael Giorgio

2. 1606895461 - William Rumanta

3. 1606879773 - Misael Jonathan

4. 1606918446 - Zulia Putri

5. 1606875996 - Nabila Fakhirah Yusroni

### Please check out our end product here!<br/>
https://frosty-leakey-31562c.netlify.com/

### Plese check out our product pitch slides here!<br/>
https://docs.google.com/presentation/d/1I2_bgy8h3WdYFQVvl1xwwugPN2ljtGO-4w-RXVU-BEA/edit?usp=sharing

![image1.png](screenshots/image1.png)
![image2.png](screenshots/image2.png)
![image3.png](screenshots/image3.png)

## About Us

Us humans, we need money. The more we have the better. One way to make money is through trade and invest.
* Our App helps you to model the price of a financial contract
* Our App gives you considerations to buy or not to buy a contract

> Most non-trivial contract payoffs depend on certain events (e.g. stock price, FX rate, etc.). Let’s say we need to price an option to buy stock in 1 year for 100$. If we knew the price of the stock in 1 year, it would be a trivial task. Of course, we can’t see the future. However, we can model the price.

### Tech Stacks

* Front End:
	* React with TypeScript, Stateful (React Hooks)

* Back End:
	* Serverless (powered by AWS Haskell Runtime), Haskell Functional Service


## User Guide

In the project directory, you can run:

### `npm install`

Run this command to install all the dependencies and packages used in the development, before start anything.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
